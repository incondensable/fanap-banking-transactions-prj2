package com.fanap.process;

public enum RsCodes {
    DEPOSIT_VALID("The process of Depositing is valid"),
    DEPOSIT_INVALID("The process of Depositing is not valid"),
    WITHDRAW_VALID("The process of Withdrawing is valid"),
    WITHDRAW_INVALID("The process of Withdrawing is not valid");

    public final String label;

    private RsCodes(String label) {
        this.label = label;
    }
}
