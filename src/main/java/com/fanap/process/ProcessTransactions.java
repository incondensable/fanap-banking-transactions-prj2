package com.fanap.process;

import com.fanap.evaluation.Evaluator;
import com.fanap.logger.LoggerConfig;
import com.fanap.model.Transactions;
import com.fanap.model.server.Deposits;
import com.fanap.repository.Result;
import com.fanap.util.IntegerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;

public class ProcessTransactions {
    private static final Logger logger = LogManager.getLogger();

    public static Result processDepositTransaction(Transactions depositTransactions, Deposits deposit) {
        LoggerConfig.initializeLogger();

        BigInteger transactionAmount = depositTransactions.getAmount();
        BigInteger depositInitialBalance = new BigInteger(deposit.getInitialBalance());
        BigInteger upperBound = new BigInteger(deposit.getUpperBound());
        int transactionId = depositTransactions.getId();
        int rsCode = Evaluator.depositTransactionValidator(
                transactionAmount,
                depositInitialBalance,
                upperBound,
                transactionId);

        if (rsCode == IntegerUtil.VALID) {

            BigInteger result = transactionAmount.add(depositInitialBalance);
            deposit.setInitialBalance(String.valueOf(result));

            logger.debug("initial balance of " + deposit.getId() + " is: " + deposit.getInitialBalance()
                    + " and its balance after depositing is: " + result);

            return new Result(
                    deposit.getCustomer(),
                    deposit.getId(),
                    deposit.getInitialBalance(),
                    depositTransactions.getId(),
                    depositTransactions.getType(),
                    result,
                    RsCodes.DEPOSIT_VALID.label
                    );
        } else {
            return new Result(
                    deposit.getCustomer(),
                    deposit.getId(),
                    deposit.getInitialBalance(),
                    depositTransactions.getId(),
                    depositTransactions.getType(),
                    depositInitialBalance,
                    RsCodes.DEPOSIT_INVALID.label
            );
        }
    }

    public static Result processWithdrawTransaction(Transactions withdrawTransactions, Deposits deposit) {
        LoggerConfig.initializeLogger();
        System.out.println("process1 " + deposit.getCustomer());

        BigInteger tranAmount = withdrawTransactions.getAmount();
        BigInteger depositAmount = new BigInteger(deposit.getInitialBalance());
        int rsCode = Evaluator.withdrawTransactionValidator(
                withdrawTransactions,
                deposit);

        if (rsCode == IntegerUtil.VALID) {
            BigInteger result = depositAmount.add(tranAmount.negate());
            deposit.setInitialBalance(String.valueOf(result));

            logger.debug("initial balance of " + deposit.getId() + " is: " + deposit.getInitialBalance()
                    + " and its balance after withdrawing is: " + result);

            System.out.println("process2 " + deposit.getCustomer());

            return new Result(
                    deposit.getCustomer(),
                    deposit.getId(),
                    deposit.getInitialBalance(),
                    withdrawTransactions.getId(),
                    withdrawTransactions.getType(),
                    result,
                    RsCodes.WITHDRAW_VALID.label
            );
        } else {
            return new Result(
                    deposit.getCustomer(),
                    deposit.getId(),
                    deposit.getInitialBalance(),
                    withdrawTransactions.getId(),
                    withdrawTransactions.getType(),
                    depositAmount,
                    RsCodes.WITHDRAW_INVALID.label
            );
        }
    }
}