package com.fanap.server;

import com.fanap.model.Transactions;
import com.fanap.model.server.Deposits;
import com.fanap.process.ProcessTransactions;
import com.fanap.repository.Result;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.util.List;
import java.util.Optional;

public class TerminalManager implements Runnable {
    private final Socket socket;
    private final List<Deposits> depositsList;

    public TerminalManager(Socket socket, List<Deposits> depositsList) {
        this.socket = socket;
        this.depositsList = depositsList;
    }

    @Override
    public void run() {
        try {
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());

            Transactions transaction = (Transactions) input.readObject();
            Deposits deposit = depositDeterminer(transaction, depositsList);

            Result result;
            if (transaction.getType().equals("deposit")) {
                result = ProcessTransactions.processDepositTransaction(transaction, deposit);
            } else {
                result = ProcessTransactions.processWithdrawTransaction(transaction, deposit);
            }
            output.writeObject(result);

            output.flush();
            output.close();
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Deposits depositDeterminer(Transactions transactions, List<Deposits> depositsList) {
        Deposits deposit = null;
        BigInteger depositId = transactions.getDeposit();

        Optional<Deposits> aDeposit = depositsList.stream()
                .filter(deposits -> depositId.equals(deposits.getId()))
                .findAny();
        if (aDeposit.isPresent()) {
            deposit = aDeposit.get();
        }
        return deposit;
    }
}