package com.fanap.server;

import com.fanap.convertor.JsonParser;
import com.fanap.logger.LoggerConfig;
import com.fanap.model.server.Deposits;
import com.fanap.util.IntegerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.*;
import java.util.List;

public class ServerConfig {
    private final Logger logger = LogManager.getLogger();
    private List<Deposits> depositsList;
    private ServerSocket serverSocket;

    public void serverSetup() {
        LoggerConfig.initializeLogger();
        try {
            depositsList = JsonParser.parseJsonObjectDeposits();
            int serverPort = JsonParser.getPort();
            serverSocket = new ServerSocket(serverPort);
            logger.debug("server has been created! port number: " + serverPort);
            int t = IntegerUtil.VALID;
            while (true) {
                Socket socket = serverSocket.accept();
                logger.debug("A Client has been connected!");
                t++;
                String threadName = "Thread-" + t;
                Thread thread = new Thread(new TerminalManager(socket, depositsList), threadName);
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}