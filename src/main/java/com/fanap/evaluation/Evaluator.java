package com.fanap.evaluation;

import com.fanap.logger.LoggerConfig;
import com.fanap.model.Transactions;
import com.fanap.model.server.Deposits;
import com.fanap.util.IntegerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;

public class Evaluator {
    private static final Logger logger = LogManager.getLogger();

    public static int depositTransactionValidator(BigInteger transactionAmount,
                                                      BigInteger depositInitialBalance,
                                                      BigInteger depositUpperBound,
                                                      int transactionId) {
        LoggerConfig.initializeLogger();

        BigInteger depositBalance = transactionAmount.add(depositInitialBalance);

        if (depositBalance.compareTo(BigInteger.ZERO) > IntegerUtil.VALID) {
            if (depositBalance.compareTo(depositUpperBound) < IntegerUtil.VALID) {
                logger.debug("its okay to process the transaction " + transactionId);
                return IntegerUtil.VALID;
            } else {
                System.out.println("");
                logger.debug("violation of transaction " + transactionId
                        + "  since the upper bound is: " + depositUpperBound
                        + " the balance is: " + depositInitialBalance);
                return IntegerUtil.INVALID;
            }
        } else {
            logger.debug("the given amount is invalid");
            return IntegerUtil.INCORRECT;
        }
    }

    public static int withdrawTransactionValidator(Transactions withdrawTransactions, Deposits deposits) {
        LoggerConfig.initializeLogger();
        BigInteger withdrawAmount = withdrawTransactions.getAmount();
        BigInteger depositInitialBalance = new BigInteger(deposits.getInitialBalance());

        if (withdrawAmount.compareTo(BigInteger.ZERO) > IntegerUtil.VALID) {
            if (withdrawAmount.compareTo(depositInitialBalance) > IntegerUtil.VALID) {
                logger.debug("the balance is insufficient! transaction id: " + withdrawTransactions.getId()
                        + " and withdraw amount is: " + withdrawTransactions.getAmount()
                        + " and the initial balance is: " + depositInitialBalance);
                return IntegerUtil.INVALID;
            } else {
                logger.debug("withdraw can be processed with id: " + withdrawTransactions.getId()
                        + " and amount of: " + withdrawTransactions.getAmount());
                return IntegerUtil.VALID;
            }
        } else {
            logger.debug("the given amount is invalid");
            return IntegerUtil.INCORRECT;
        }
    }
}