package com.fanap.model.server;

import java.math.BigInteger;

public class Deposits {
    private String customer;
    private BigInteger id;
    private String initialBalance;
    private String upperBound;

    public String getCustomer() {
        return customer;
    }

    public BigInteger getId() {
        return id;
    }

    public String getInitialBalance() {
        return initialBalance;
    }

    public String getUpperBound() {
        return upperBound;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public void setInitialBalance(String initialBalance) {
        this.initialBalance = initialBalance;
    }

    public void setUpperBound(String upperBound) {
        this.upperBound = upperBound;
    }
}
