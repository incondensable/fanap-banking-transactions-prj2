package com.fanap.convertor;

import com.fanap.logger.LoggerConfig;
import com.fanap.model.server.Deposits;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class JsonParser {
    private static final Logger logger = LogManager.getLogger();
    public static int port;
    public static String outLog;

    public static List<Deposits> parseJsonObjectDeposits() {
        LoggerConfig.initializeLogger();
        List<Deposits> depositsList = new ArrayList<>();

        JSONParser jsonParser = new JSONParser();

        try (FileReader fileReader = new FileReader("/Users/incondensable/Desktop/prj2/resource/core.json")) {

            JSONObject jsonObject = (JSONObject) jsonParser.parse(fileReader);
            fileReader.close();

            port = Integer.parseInt(jsonObject.get("port").toString());
            outLog = jsonObject.get("outLog").toString();

            JSONArray depositsArray = (JSONArray) jsonObject.get("deposits");

            int i = 0;
            for (Object o : depositsArray.toArray()) {
                JSONObject deposit = (JSONObject) o;
                String customer = deposit.get("customer").toString();
                BigInteger id = BigInteger.valueOf(Long.parseLong(deposit.get("id").toString()));
                String initialBalance = deposit.get("initialBalance").toString();
                String upperBound = deposit.get("upperBound").toString();

                if (i < depositsArray.size()) {
                    Deposits deposits = new Deposits();
                    deposits.setCustomer(customer);
                    deposits.setId(id);
                    deposits.setInitialBalance(initialBalance);
                    deposits.setUpperBound(upperBound);

                    depositsList.add(deposits);
                    i++;
                }
            }
        } catch (Exception e) {
            logger.debug(e);
        }
        return depositsList;
    }

    public static int getPort() {
        return port;
    }

    public static String getOutLog() {
        return outLog;
    }
}